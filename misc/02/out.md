# Output of Practicing `super()` and `MRO`

* By Vector Li <huanli@redhat.com>
* On 2022/6/7

* **NOTES**:
  1. **Method Resolution Order (MRO)** is the order in which methods should be
     inherited in the presence of multiple inheritance. You can view the MRO
     by using the `__mro__` attribute.
     方法解析顺序(MRO)是在存在多重继承的情况下继承方法的顺序。
     可以通过`__mro__`属性查看MRO.
  2. To display `__mro__`, you must use `<Class name>.__mro__`.
     That is, `object.__mro__` will lead to error `AttributeError`.

## 1 - Execute foo.py
```bash
$ python3 foo.py
Dog has 4 legs.
Dog can't swim.
Dog can't fly.
Dog is a warm-blooded animal.
Dog is an animal.
--------------------------------------------------------------------------------
(<class '__main__.Dog'>,
 <class '__main__.NonMarineMammal'>,
 <class '__main__.NonWingedMammal'>,
 <class '__main__.Mammal'>,
 <class '__main__.Animal'>,
 <class 'object'>)
--------------------------------------------------------------------------------
Bat can't swim.
Bat is a warm-blooded animal.
Bat is an animal.
```

## 2 - Debug foo.py

```python
$ python3 -m pdb foo.py
(Pdb) l 45
 40     def main(argc, argv):
 41         dog = Dog()
 42         print('-' * 80)
 43         pprint(Dog.__mro__)
 44         print('-' * 80)
 45         bat = NonMarineMammal('Bat')
 46
 47
 48     if __name__ == '__main__':
 49         sys.exit(main(len(sys.argv), sys.argv))
[EOF]
(Pdb) b 41
Breakpoint 1 at /var/tmp/huanli/001-python-cookbook/misc/02/foo.py:41
(Pdb) b 45
Breakpoint 2 at /var/tmp/huanli/001-python-cookbook/misc/02/foo.py:45
(Pdb) r
> /var/tmp/huanli/001-python-cookbook/misc/02/foo.py(41)main()
-> dog = Dog()
(Pdb) n
Dog has 4 legs.
Dog can't swim.
Dog can't fly.
Dog is a warm-blooded animal.
Dog is an animal.
> /var/tmp/huanli/001-python-cookbook/misc/02/foo.py(42)main()
-> print('-' * 80)
(Pdb)
(Pdb) ### 01 - To display __mro__, we must use the class name !! ##############
(Pdb) pp dog.__mro__
*** AttributeError: 'Dog' object has no attribute '__mro__'
(Pdb)
(Pdb)
(Pdb) ### 02 - Display Dog.__mro__  ###########################################
(Pdb) pp Dog.__mro__
(<class '__main__.Dog'>,
 <class '__main__.NonMarineMammal'>,
 <class '__main__.NonWingedMammal'>,
 <class '__main__.Mammal'>,
 <class '__main__.Animal'>,
 <class 'object'>)
(Pdb) c
--------------------------------------------------------------------------------
(<class '__main__.Dog'>,
 <class '__main__.NonMarineMammal'>,
 <class '__main__.NonWingedMammal'>,
 <class '__main__.Mammal'>,
 <class '__main__.Animal'>,
 <class 'object'>)
--------------------------------------------------------------------------------
> /var/tmp/huanli/001-python-cookbook/misc/02/foo.py(45)main()
-> bat = NonMarineMammal('Bat')
(Pdb)
(Pdb) ### 03 - Display NonMarineMammal.__mro__  ###############################
(Pdb) pp NonMarineMammal.__mro__
(<class '__main__.NonMarineMammal'>,
 <class '__main__.Mammal'>,
 <class '__main__.Animal'>,
 <class 'object'>)
(Pdb) n
Bat can't swim.
Bat is a warm-blooded animal.
Bat is an animal.
--Return--
> /var/tmp/huanli/001-python-cookbook/misc/02/foo.py(45)main()->None
-> bat = NonMarineMammal('Bat')
(Pdb)
(Pdb) l 11, 45
 11     class Animal:
 12         def __init__(self, Animal):
 13             print(Animal, 'is an animal.');
 14
 15
 16     class Mammal(Animal):
 17         def __init__(self, mammalName):
 18             print(mammalName, 'is a warm-blooded animal.')
 19             super().__init__(mammalName)
 20
 21
 22     class NonWingedMammal(Mammal):
 23         def __init__(self, NonWingedMammal):
 24             print(NonWingedMammal, "can't fly.")
 25             super().__init__(NonWingedMammal)
 26
 27
 28     class NonMarineMammal(Mammal):
 29         def __init__(self, NonMarineMammal):
 30             print(NonMarineMammal, "can't swim.")
 31             super().__init__(NonMarineMammal)
 32
 33
 34     class Dog(NonMarineMammal, NonWingedMammal):
 35         def __init__(self):
 36             print('Dog has 4 legs.');
 37             super().__init__('Dog')
 38
 39
 40     def main(argc, argv):
 41 B       dog = Dog()
 42         print('-' * 80)
 43         pprint(Dog.__mro__)
 44         print('-' * 80)
 45 B->     bat = NonMarineMammal('Bat')
(Pdb)
(Pdb)
(Pdb) #########################################################################
(Pdb) # 有了MRO, 在执行L41行Dog()时，L18行就不可能(如乍一看)被执行两次!!! #####
(Pdb) #########################################################################
(Pdb)
(Pdb)
```
