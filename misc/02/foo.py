#!/usr/bin/python3
"""
Sample script to learn super() and MRO (Method Resolution Order)

FROM: https://www.programiz.com/python-programming/methods/built-in/super
"""

import sys
from pprint import pprint

class Animal:
    def __init__(self, Animal):
        print(Animal, 'is an animal.');


class Mammal(Animal):
    def __init__(self, mammalName):
        print(mammalName, 'is a warm-blooded animal.')
        super().__init__(mammalName)


class NonWingedMammal(Mammal):
    def __init__(self, NonWingedMammal):
        print(NonWingedMammal, "can't fly.")
        super().__init__(NonWingedMammal)


class NonMarineMammal(Mammal):
    def __init__(self, NonMarineMammal):
        print(NonMarineMammal, "can't swim.")
        super().__init__(NonMarineMammal)


class Dog(NonMarineMammal, NonWingedMammal):
    def __init__(self):
        print('Dog has 4 legs.');
        super().__init__('Dog')


def main(argc, argv):
    dog = Dog()
    print('-' * 80)
    pprint(Dog.__mro__)
    print('-' * 80)
    bat = NonMarineMammal('Bat')


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
