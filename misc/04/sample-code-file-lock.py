""" 
Doc: https://docs.python.org/3/library/fcntl.html
Ref: https://github.com/teemtee/tmt/issues/1269#issuecomment-1162863773
Ref: https://programtalk.com/python-examples/fcntl.flock/
"""

# FROM: https://pagure.io/testcloud/blob/master/f/testcloud/util.py#_196
class Filelock(object):
    def __init__(self, timeout=25, wait_time=0.5):
        # We need to define the lock_path here so it won't get overwritten by importing tc's config in this file
        self.lock_path = os.path.join(config_data.DATA_DIR, 'testcloud.lock')
        self.fd = open(self.lock_path, 'w+')
        self.timeout = timeout
        self.wait_time = wait_time
	
    def __enter__(self):
        start_time = time.time()
        while 1:
            try:
                fcntl.lockf(self.fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
                log.debug("Lock acquired")
                break
            except (OSError, IOError) as ex:
                if ex.errno == errno.EAGAIN:
                    log.debug("Waiting for lock")
                    time.sleep(self.wait_time)
                else:
                    raise ex
	
            if (start_time + self.timeout) <= time.time():
                log.debug("Lock timeout reached")
                break
	
    def __exit__(self, exc_type, exc_val, exc_tb):
        fcntl.lockf(self.fd, fcntl.LOCK_UN)
        log.debug("Lock lifted")
	
    def __del__(self):
        self.fd.close()
