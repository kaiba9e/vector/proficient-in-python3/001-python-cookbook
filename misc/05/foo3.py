#!/usr/bin/python3

def bold(func):
    def newfunc(args):
        old_result = func(args)
        new_result = '<b>' + old_result +'</b>'
        return new_result
    return newfunc

def upper(func):
    def newfunc(args):
        old_result = func(args)
        new_result = old_result.upper()
        return new_result
    return newfunc

@bold
@upper
def greeting(s):
    return s

import debug; debug.init()
output = greeting('Hello World')
print(output)
