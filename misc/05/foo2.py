#!/usr/bin/python3

def upper(func):
    def newfunc(args):
        old_result = func(args)
        new_result = old_result.upper()
        return new_result
    return newfunc

def greeting(s):
    return s

import debug; debug.init()
igreeting = upper(greeting)
output = igreeting('Hello World')
print(output)
