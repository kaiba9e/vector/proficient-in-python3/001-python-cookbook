#!/usr/bin/python
import sys

def a(func):
    print(">>> Call decorator a() ...", file=sys.stderr)
    def newfunc(args):
        return func(args) + 'a'
    return newfunc

def b(func):
    print(">>> Call decorator b() ...", file=sys.stderr)
    def newfunc(args):
        return func(args) + 'b'
    return newfunc

def c(func):
    print(">>> Call decorator c() ...", file=sys.stderr)
    def newfunc(args):
        return func(args) + 'c'
    return newfunc

@a
@b
@c
def func(s):
    return s

out = func('abc:')
print(out)
# Output to stdout: 'abc:cba'
