```bash
huanli@ThinkPadTV:05$ cat -n foo1.py
     1	#!/usr/bin/python3
     2
     3	def upper(func):
     4	    def newfunc(args):
     5	        old_result = func(args)
     6	        new_result = old_result.upper()
     7	        return new_result
     8	    return newfunc
     9
    10	@upper
    11	def greeting(s):
    12	    return s
    13
    14	import debug; debug.init()
    15	output = greeting('Hello World')
    16	print(output)

huanli@ThinkPadTV:05$ cat -n foo2.py
     1	#!/usr/bin/python3
     2
     3	def upper(func):
     4	    def newfunc(args):
     5	        old_result = func(args)
     6	        new_result = old_result.upper()
     7	        return new_result
     8	    return newfunc
     9
    10	def greeting(s):
    11	    return s
    12
    13	import debug; debug.init()
    14	igreeting = upper(greeting)
    15	output = igreeting('Hello World')
    16	print(output)

huanli@ThinkPadTV:05$
huanli@ThinkPadTV:05$ diff -u foo1.py  foo2.py
--- foo1.py	2022-07-10 18:41:30.429902569 +0800
+++ foo2.py	2022-07-10 18:43:00.305110175 +0800
@@ -7,10 +7,10 @@
         return new_result
     return newfunc

-@upper
 def greeting(s):
     return s

 import debug; debug.init()
-output = greeting('Hello World')
+igreeting = upper(greeting)
+output = igreeting('Hello World')
 print(output)
huanli@ThinkPadTV:05$

huanli@ThinkPadTV:05$ export PYTHONPATH=/var/tmp:$PYTHONPATH
huanli@ThinkPadTV:05$ export DEBUG=yes

huanli@ThinkPadTV:05$ python3 foo1.py
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo1.py:4|0]++ def newfunc{'args': 'Hello World', 'func': <function greeting at 0x7f823cf4c550>}:
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo1.py:5|0]+         old_result = func(args)
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo1.py:10|0]++ def greeting{'s': 'Hello World'}:
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo1.py:12|0]+     return s
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo1.py:12|0]++ <RETURN> ++ Hello World
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo1.py:6|0]+         new_result = old_result.upper()
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo1.py:7|0]+         return new_result
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo1.py:7|0]++ <RETURN> ++ HELLO WORLD
HELLO WORLD
__DEBUG__: [fini()@/var/tmp/debug.py:375|0]++ def fini{}:
__DEBUG__: [fini()@/var/tmp/debug.py:387|0]+     sys.settrace(None)
huanli@ThinkPadTV:05$

huanli@ThinkPadTV:05$ python3 foo2.py
__DEBUG__: [upper()@/var/tmp/001-python-cookbook/misc/05/foo2.py:3|0]++ def upper{'func': <function greeting at 0x7f2897250550>}:
__DEBUG__: [upper()@/var/tmp/001-python-cookbook/misc/05/foo2.py:4|0]+     def newfunc(args):
__DEBUG__: [upper()@/var/tmp/001-python-cookbook/misc/05/foo2.py:8|0]+     return newfunc
__DEBUG__: [upper()@/var/tmp/001-python-cookbook/misc/05/foo2.py:8|0]++ <RETURN> ++ <function upper.<locals>.newfunc at 0x7f2897250670>
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo2.py:4|0]++ def newfunc{'args': 'Hello World', 'func': <function greeting at 0x7f2897250550>}:
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo2.py:5|0]+         old_result = func(args)
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo2.py:10|0]++ def greeting{'s': 'Hello World'}:
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo2.py:11|0]+     return s
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo2.py:11|0]++ <RETURN> ++ Hello World
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo2.py:6|0]+         new_result = old_result.upper()
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo2.py:7|0]+         return new_result
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo2.py:7|0]++ <RETURN> ++ HELLO WORLD
HELLO WORLD
__DEBUG__: [fini()@/var/tmp/debug.py:375|0]++ def fini{}:
__DEBUG__: [fini()@/var/tmp/debug.py:387|0]+     sys.settrace(None)
huanli@ThinkPadTV:05$


###################################################################################################

huanli@ThinkPadTV:05$ cat -n foo3.py
     1	#!/usr/bin/python3
     2
     3	def bold(func):
     4	    def newfunc(args):
     5	        old_result = func(args)
     6	        new_result = '<b>' + old_result +'</b>'
     7	        return new_result
     8	    return newfunc
     9
    10	def upper(func):
    11	    def newfunc(args):
    12	        old_result = func(args)
    13	        new_result = old_result.upper()
    14	        return new_result
    15	    return newfunc
    16
    17	@bold
    18	@upper
    19	def greeting(s):
    20	    return s
    21
    22	import debug; debug.init()
    23	output = greeting('Hello World')
    24	print(output)
huanli@ThinkPadTV:05$
huanli@ThinkPadTV:05$ diff -u foo1.py foo3.py
--- foo1.py	2022-07-10 18:54:30.021703402 +0800
+++ foo3.py	2022-07-10 18:57:58.135184138 +0800
@@ -1,5 +1,12 @@
 #!/usr/bin/python3

+def bold(func):
+    def newfunc(args):
+        old_result = func(args)
+        new_result = '<b>' + old_result +'</b>'
+        return new_result
+    return newfunc
+
 def upper(func):
     def newfunc(args):
         old_result = func(args)
@@ -7,6 +14,7 @@
         return new_result
     return newfunc

+@bold
 @upper
 def greeting(s):
     return s
huanli@ThinkPadTV:05$
huanli@ThinkPadTV:05$ unset DEBUG
huanli@ThinkPadTV:05$ python3 foo3.py
<b>HELLO WORLD</b>
huanli@ThinkPadTV:05$ export DEBUG=yes
huanli@ThinkPadTV:05$ python3 foo3.py
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:4|0]++ def newfunc{'args': 'Hello World', 'func': <function upper.<locals>.newfunc at 0x7f70b9e55670>}:
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:5|0]+         old_result = func(args)
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:11|0]++ def newfunc{'args': 'Hello World', 'func': <function greeting at 0x7f70b9e555e0>}:
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:12|0]+         old_result = func(args)
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo3.py:17|0]++ def greeting{'s': 'Hello World'}:
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo3.py:20|0]+     return s
__DEBUG__: [greeting()@/var/tmp/001-python-cookbook/misc/05/foo3.py:20|0]++ <RETURN> ++ Hello World
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:13|0]+         new_result = old_result.upper()
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:14|0]+         return new_result
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:14|0]++ <RETURN> ++ HELLO WORLD
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:6|0]+         new_result = '<b>' + old_result +'</b>'
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:7|0]+         return new_result
__DEBUG__: [newfunc()@/var/tmp/001-python-cookbook/misc/05/foo3.py:7|0]++ <RETURN> ++ <b>HELLO WORLD</b>
<b>HELLO WORLD</b>
__DEBUG__: [fini()@/var/tmp/debug.py:375|0]++ def fini{}:
__DEBUG__: [fini()@/var/tmp/debug.py:387|0]+     sys.settrace(None)
huanli@ThinkPadTV:05$

###################################################################################################

huanli@ThinkPadTV:05$ python3 fooStack.py
>>> Call decorator c() ...
>>> Call decorator b() ...
>>> Call decorator a() ...
abc:cba

huanli@ThinkPadTV:05$ python3 -m pdb /tmp/fooStack.py
> /tmp/fooStack.py(2)<module>()
-> import sys
(Pdb) b 28
Breakpoint 1 at /tmp/fooStack.py:28
(Pdb) l 22,28
 22  	@a
 23  	@b
 24  	@c
 25  	def func(s):
 26  	    return s
 27
 28 B	out = func('abc:')
(Pdb) r
>>> Call decorator c() ...
>>> Call decorator b() ...
>>> Call decorator a() ...
> /tmp/fooStack.py(28)<module>()
-> out = func('abc:')
(Pdb) n
> /tmp/fooStack.py(29)<module>()
-> print(out)
(Pdb) p out
'abc:cba'
(Pdb) q

```
