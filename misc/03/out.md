## 01 - output
```bash
$ python3 foo.py
Item('bar')
Item('spam')
Item('foo')
Item('grok')
```

## 02 - output of pdb

```python
$ python3 -m pdb foo.py
...<snip>...
(Pdb) l
 34  	    q.push(Item('foo'), 1)
 35  	    q.push(Item('bar'), 5)
 36  	    q.push(Item('spam'), 4)
 37  	    q.push(Item('grok'), 1)
 38  	
 39  	    print(q.pop())
 40  	    print(q.pop())
 41  	    print(q.pop())
 42  	    print(q.pop())
 43  	
 44  	    return 0
(Pdb) b 39
Breakpoint 1 at /tmp/FOO/misc/03/foo.py:39
(Pdb) b
Num Type         Disp Enb   Where
1   breakpoint   keep yes   at /tmp/FOO/misc/03/foo.py:39
(Pdb) r
> /tmp/FOO/misc/03/foo.py(39)main()
-> print(q.pop())
(Pdb) p q
<__main__.PriorityQueue object at 0x7ffb6234eeb8>
(Pdb) pp q.__dict__
{'_index': 4,
 '_queue': [(-5, 1, Item('bar')),
            (-1, 0, Item('foo')),
            (-4, 2, Item('spam')),
            (-1, 3, Item('grok'))]}
(Pdb) n
Item('bar')
> /tmp/FOO/misc/03/foo.py(40)main()
-> print(q.pop())
(Pdb) s
--Call--
> /tmp/FOO/misc/03/foo.py(17)pop()
-> def pop(self):
(Pdb) n
> /tmp/FOO/misc/03/foo.py(18)pop()
-> *_, item = heapq.heappop(self._queue)
(Pdb) n
> /tmp/FOO/misc/03/foo.py(19)pop()
-> self._index -= 1
(Pdb) pp _
[-4, 2]
(Pdb) p item
Item('spam')
(Pdb) pp self._queue
[(-1, 0, Item('foo')), (-1, 3, Item('grok'))]
(Pdb) n
> /tmp/FOO/misc/03/foo.py(20)pop()
-> return item
(Pdb) n
--Return--
> /tmp/FOO/misc/03/foo.py(20)pop()->Item('spam')
-> return item
(Pdb) n
--Call--
> /tmp/FOO/misc/03/foo.py(27)__repr__()
-> def __repr__(self):
(Pdb) 
> /tmp/FOO/misc/03/foo.py(28)__repr__()
-> return 'Item({!r})'.format(self.name)
(Pdb) 
--Return--
> /tmp/FOO/misc/03/foo.py(28)__repr__()->"Item('spam')"
-> return 'Item({!r})'.format(self.name)
(Pdb) 
Item('spam')
> /tmp/FOO/misc/03/foo.py(41)main()
-> print(q.pop())
(Pdb) 
Item('foo')
> /tmp/FOO/misc/03/foo.py(42)main()
-> print(q.pop())
(Pdb) 
Item('grok')
> /tmp/FOO/misc/03/foo.py(44)main()
-> return 0
(Pdb) 
```
