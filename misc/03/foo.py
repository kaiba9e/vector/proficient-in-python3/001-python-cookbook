#!/usr/bin/python3

""" p9 1.5.2 """

import sys
import heapq

class PriorityQueue(object):
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        *_, item = heapq.heappop(self._queue)
        self._index -= 1
        return item 


class Item(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'Item({!r})'.format(self.name)


def main(argc, argv):
    q = PriorityQueue()

    q.push(Item('foo'), 1)
    q.push(Item('bar'), 5)
    q.push(Item('spam'), 4)
    q.push(Item('grok'), 1)

    print(q.pop())
    print(q.pop())
    print(q.pop())
    print(q.pop())

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
