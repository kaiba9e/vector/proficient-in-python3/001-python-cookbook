* [05](05/foo1.py) - `2022/7/10` - `decorator`
  * [Multiple decorators](05/foo3.py)
    * 一个函数可以有多个装饰器。设定的方法是在函数上方设定装饰器函数。当有多个
      装饰器函数时，执行顺序为从下往上依次执行。例如：
      ```python
      @foo1
      @foo2
      @foo3
      def func(s):
          return s

      #########################################################################
      # The call stack looks like:
      #     foo3
      #       -> foo2
      #            -> foo1
      #########################################################################
      ```

* [04](04/sample-code-file-lock.py) - `2022/6/22` - `file lock`

* [03](03/foo.py) - `2022/6/19` - `heapq and priority queue`


* [02](02/foo.py) - `2022/6/7` - `super()`
> The super() builtin returns a proxy object (temporary object of the
> superclass) that allows us to access methods of the base class.
> For more, please visit [Python super\(\)][02A]

[02A]: https://www.programiz.com/python-programming/methods/built-in/super


* [01](01/foo.py) - `2022/6/6` - `object.__dict`
> Get all properties of an object
