#!/usr/bin/python3

""" Use object.__dict__ to check all properties """

import sys


class Foo(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def output(self):
        print("name: %s" % self.name)
        print("age : %d" % self.age)


class Bar(Foo):
    def __init__(self, name, age, gender):
        super().__init__(name, age)
        self.gender = gender

    def output(self):
        super().output()
        print("gender: %s" % self.gender)


def main(argc, argv):
    # import pdb; pdb.set_trace()
    foo = Foo('FDN', 20)
    foo.output()

    bar = Bar('BHU', 30, 'Female')
    bar.output()

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
