# Output of Practicing `object.__dict__`

* By Vector Li <huanli@redhat.com>
* On 2022/6/6

## 1 - Execute foo.py
```bash
$ python3 foo.py
name: FDN
age : 20
name: BHU
age : 30
gender: Female
```

## 2 - Debug foo.py

```python
$ python3 -m pdb foo.py
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(3)<module>()
-> """ Use object.__dict__ to check all properties """
(Pdb) l 28
 23         def output(self):
 24             super().output()
 25             print("gender: %s" % self.gender)
 26
 27
 28     def main(argc, argv):
 29         # import pdb; pdb.set_trace()
 30         foo = Foo('FDN', 20)
 31         foo.output()
 32
 33         bar = Bar('BHU', 30, 'Female')
(Pdb) b 30
Breakpoint 1 at /var/tmp/huanli/001-python-cookbook/misc/01/foo.py:30
(Pdb) b 33
Breakpoint 2 at /var/tmp/huanli/001-python-cookbook/misc/01/foo.py:33
(Pdb) b
Num Type         Disp Enb   Where
1   breakpoint   keep yes   at /var/tmp/huanli/001-python-cookbook/misc/01/foo.py:30
2   breakpoint   keep yes   at /var/tmp/huanli/001-python-cookbook/misc/01/foo.py:33
(Pdb) r
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(30)main()
-> foo = Foo('FDN', 20)
(Pdb) n
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(31)main()
-> foo.output()
(Pdb)
(Pdb)
(Pdb) ########## display the properties of `foo` ##############################
(Pdb) pp foo.__dict__
{'age': 20, 'name': 'FDN'}
(Pdb)
(Pdb) n
name: FDN
age : 20
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(33)main()
-> bar = Bar('BHU', 30, 'Female')
(Pdb) n
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(34)main()
-> bar.output()
(Pdb)
(Pdb)
(Pdb) ########## display the properties of `bar` ##############################
(Pdb) pp bar.__dict__
{'age': 30, 'gender': 'Female', 'name': 'BHU'}
(Pdb)
(Pdb) pp dir(bar)
['__class__',
 '__delattr__',
 '__dict__',
 ...........................<snip>.............................................
 '__weakref__',
 'age',
 'gender',
 'name',
 'output']
(Pdb)
(Pdb) # NOTE: From dir(bar), we can also see the method `output`, which is not
(Pdb) #       wanted because we just want to see all properties of `bar`.
(Pdb) #       Hence, it is very useful to use `bar.__dict__` !!!
(Pdb)
(Pdb) c
name: BHU
age : 30
gender: Female
The program exited via sys.exit(). Exit status: 0
> /var/tmp/huanli/001-python-cookbook/misc/01/foo.py(3)<module>()
-> """ Use object.__dict__ to check all properties """
(Pdb) q
$
```
